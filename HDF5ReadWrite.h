#ifndef _HDF5_read_write_h_
#define _HDF5_read_write_h_

#include <H5Cpp.h>
#include <type_traits>
#include <iostream>
#include <algorithm>
#include <vector>

// define important read/write functions, since the HDF5/C++ API is complete shit
/*
    authors:
    Max Stadelmaier, Darko Veberic

*/

template<typename CppType> struct MapH5 { typedef H5::PredType Type; };
template<> struct MapH5<std::string> { typedef H5::StrType Type; };

template<typename T> struct Data { static const typename MapH5<T>::Type& Type(); };

template<> struct Data<double> { static const H5::PredType& Type() { return H5::PredType::NATIVE_DOUBLE; } };
template<> struct Data<float> { static const H5::PredType& Type() { return H5::PredType::NATIVE_FLOAT; } };
template<> struct Data<int> { static const H5::PredType& Type() { return H5::PredType::NATIVE_INT; } };
template<> struct Data<std::string> { static const H5::StrType& Type() { static H5::StrType t(H5::PredType::C_S1, 256); return t; } };


template<typename T>
bool
Write(H5::Group& group, const T& number, const std::string& name)
{
  // write one item
  try {
    const auto& datatype = Data<typename std::remove_const<T>::type>::Type();
    const hsize_t dimsf[1] = { 1 };
    const H5::DataSpace dataspace(1, dimsf);  // RANK, dims
    auto dataset = group.createDataSet(name, datatype, dataspace);
    dataset.write(&number, datatype);
    dataset.close();
    return false;
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
              << name << std::endl;
    return true;
  }
}


template<typename T>
bool
Write(H5::Group& group, std::vector<T> array, const std::string& name)
{
  // write many
  try {
    const auto& datatype = Data<typename std::remove_const<T>::type>::Type();
    const hsize_t dimsf[2] = { 1, array.size() };
    const H5::DataSpace dataspace(2, dimsf);
    auto dataset = group.createDataSet(name, datatype, dataspace);
    dataset.write(&array[0], datatype);
    dataset.close();
    return false;
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
         << name << std::endl;
    return true;
  }
}


template<typename T>
bool
Write(H5::H5File& file, const T& number, const std::string& name)
{
  // write one item
  try {
    const auto& datatype = Data<typename std::remove_const<T>::type>::Type();
    const hsize_t dimsf[1] = { 1 };
    const H5::DataSpace dataspace(1, dimsf);  // RANK, dims
    auto dataset = file.createDataSet(name, datatype, dataspace);
    dataset.write(&number, datatype);
    dataset.close();
    return false;
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
              << name << std::endl;
    return true;
  }
}


template<typename T>
bool
Write(H5::H5File& file, std::vector<T> array, const std::string& name)
{
  // write many
  try {
    const auto& datatype = Data<typename std::remove_const<T>::type>::Type();
    const hsize_t dimsf[2] = { 1, array.size() };
    const H5::DataSpace dataspace(2, dimsf);
    auto dataset = file.createDataSet(name, datatype, dataspace);
    dataset.write(&array[0], datatype);
    dataset.close();
    return false;
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
         << name << std::endl;
    return true;
  }
}


inline
ssize_t
ReadDatasetLength(H5::Group& group, const std::string& name)
{
  try {
    const auto& dataset = group.openDataSet(name);
    hsize_t dimensions[2] = { 0 };  // RANK can be bigger than 2, but must not be smaller than actual dim (here 2!).
    const auto& dataspace = dataset.getSpace();
    dataspace.getSimpleExtentDims(dimensions, nullptr);
    return std::max({dimensions[0], dimensions[1]}); // don't know this is necessary here
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
              << name << std::endl;
    return -1;
  }
}


inline
ssize_t
ReadDatasetLength(H5::H5File& file, const std::string& name)
{
  try {
    const auto& dataset = file.openDataSet(name);
    hsize_t dimensions[2] = { 0 };  // RANK can be bigger than 2, but must not be smaller than actual dim (here 2!).
    const auto& dataspace = dataset.getSpace();
    dataspace.getSimpleExtentDims(dimensions, nullptr);
    return std::max({dimensions[0], dimensions[1]}); // don't know this is necessary here
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
              << name << std::endl;
    return -1;
  }
}


template<typename T>
bool
Read(H5::Group& group, T* const array, const std::string& name)
{
  try {
    const auto& dataset = group.openDataSet(name);
    hsize_t dimensions[2] = { 0 };  // RANK can be bigger than 2, but must not be smaller than actual dim (here 2!).
    const auto& dataspace = dataset.getSpace();
    dataspace.getSimpleExtentDims(dimensions, nullptr);
    dataset.read(array, Data<typename std::remove_const<T>::type>::Type());
    return false;
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
              << name << std::endl;
    return true;
  }
}


template<typename T>
bool
Read(H5::H5File& file, T* const array, const std::string& name)
{
  try {
    const auto& dataset = file.openDataSet(name);
    hsize_t dimensions[2] = { 0 };  // RANK can be bigger than 2, but must not be smaller than actual dim (here 2!).
    const auto& dataspace = dataset.getSpace();
    dataspace.getSimpleExtentDims(dimensions, nullptr);
    dataset.read(array, Data<typename std::remove_const<T>::type>::Type());
    return false;
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
              << name << std::endl;
    return true;
  }
}


template<class H5FileOrGroup>
bool
CreateOrOpenGroup(H5FileOrGroup& parent, H5::Group& newgroup, const std::string& name)
{
  try {
    newgroup = parent.exists(name) ? parent.openGroup(name) : parent.createGroup(name);
    return false;
  } catch (...) {
    std::cout << "\nHDF5 operation failed for the following object:\n"
              << name << std::endl;
    return true;
  }
}

#endif

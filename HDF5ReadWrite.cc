#include "HDF5ReadWrite.h"
#include <cmath>
#include <iostream>
#include <vector>

int main() {

  const std::string fileName = "./example_data.h5";
  H5::H5File file = H5::H5File(fileName, H5F_ACC_TRUNC);

  // write vector data
  std::vector<double> x = {1,2,3,4,5};
  Write(file, x, "x_data");

  // read data
  const hsize_t length = ReadDatasetLength(file, "x_data");
  std::vector<double> x_read(length);

  Read(file, &x_read.front(), "x_data"); // fill from first entry

  for (const auto i : x_read)
    std::cout << i << " ";
  std::cout << std::endl;

  return 0;
}
